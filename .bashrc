#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1="\e[38;5;202m\w\e[38;5;74m\$(git_branch) \[\033[1;32m\]↪\[\033[00m\] "

CODELAB="$HOME/Codelab"
DOTFILES="$HOME/dotfiles"
HOMESTEAD="$HOME/Codelab/Homestead"

export EDITOR="vim"

#--------------------------------------------------------
# Show current git branch if any
#--------------------------------------------------------
git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

function dotbak ()
{
	cp $HOME/.bashrc $HOME/dotfiles/ &&
	cp $HOME/.bash_aliases $HOME/dotfiles/ &&
	cp -r $HOME/.ssh $HOME/dotfiles/
}

#--------------------------------------------------------
# ARCHIVE EXTRACTION
#--------------------------------------------------------
function ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac 
  else
    echo "'$1' is not a valid file"
  fi
}

#--------------------------------------------------------
# Brightness
#--------------------------------------------------------
function bright {
    if [ "$1" ]; then
	if [ "$1" == 1 ]; then
	    val=1
	else
	    val="0.$1"
	fi
	xrandr | grep " connected" | cut -f1 -d " " | while read -r monitor ; do
    	    xrandr --output $monitor --brightness $val;
	done
    else
        xrandr | grep " connected" | cut -f1 -d " ";
    fi
}

#--------------------------------------------------------------
# Go to docker container shell
#-------------------------------------------------------------
function dkrit {
	if [ -z "$1" ]; then
		echo "Enter a container name or id"
	else
		docker exec -it $1 bash
	fi
}

#--------------------------------------------------------------
# Curl as googlebot
#-------------------------------------------------------------
function googlebot {
    if [ -z "$1" ]; then
        echo "Add domain"
    else
	    curl -A Googlebot "$1"
	    echo "\n"
    fi
}

#--------------------------------------------------------------
# xclip file contents
#-------------------------------------------------------------
function xcp {
    if [ "$1" ] && [ -f "$1" ]; then
        cat $1 | xclip -selection clipboard
    else
        echo "no file specified"
    fi
}

#--------------------------------------------------------
# ALIASES
#--------------------------------------------------------
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

export PATH=/opt/phpstorm/bin:/$HOME/.local/bin:$PATH

neofetch 


