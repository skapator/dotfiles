#
# ~/.bash_aliases
#

#----------------------------------------------------------
# COMMON
#----------------------------------------------------------
alias e="$EDITOR"
alias clr="clear"
alias ..="cd .."
alias ...="cd ../.."
alias eh="sudo vim /etc/hosts"
alias brc="vim ~/.bashrc"
alias sbrc="source ~/.bashrc"
alias rmf="rm -rf"
alias rmv="rm -rfv"
alias yyu="sudo pacman -Syyu"
alias shut="shutdown now"
alias reb="sudo reboot"
alias dotf="cd $DOTFILES"
alias ah="sudo -i vim /etc/hosts &"
alias v="vim"
alias jctl="journalctl -p 3 -xb"
alias ll="ls -alhF"
alias s="startx"

alias rb="sudo reboot"
alias shut="sudo poweroff"

alias pmq="pacman -Q"
alias pms="pacman -Ss"
alias pm="sudo pacman -S"
#----------------------------------------------------------
# YOUTUBE-DL
#----------------------------------------------------------
alias yt="youtube-dl"
alias ytd="youtube-dl -o '%(title)s.%(ext)s' -ic"
alias yta="youtube-dl -i --extract-audio --audio-format mp3 --audio-quality 0"
alias ytf="youtube-dl -F"
alias ytf720="youtube-dl -f 136 --get-filename -o '%(title)s-%(channel)s-%(id)s.%(ext)s'"
alias ytp="youtube-dl --yes-playlist"
alias ytap="youtube-dl -ict --yes-playlist --extract-audio --audio-format mp3 --audio-quality 0"
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias ytflac="youtube-dl --extract-audio --audio-format flac "
alias ytwav="youtube-dl --extract-audio --audio-format wav "
alias ytv="youtube-dl -f bestvideo+bestaudio "

#----------------------------------------------------------
# HOMESTEAD
#----------------------------------------------------------
alias hmstd="cd $HOMESTEAD"
alias hyml="e $HOMESTEAD/Homestead.yaml"
alias hsu="cd $HOMESTEAD && vagrant up"
alias hsh="cd $HOMESTEAD && vagrant halt"
alias hsup="cd $HOMESTEAD && vagrant up --provision"
alias hsr="cd $HOMESTEAD && vagrant reload"
alias hsrp="cd $HOMESTEAD && vagrant reload --provision"
alias hss="cd $HOMESTEAD && vagrant status"
alias vssh="vagrant ssh"
alias vup="vagrant up"
alias vups="vagrant up && vagrant ssh"
alias vh="vagrant halt"
alias art="php artisan"
alias laranew="composer create-project --prefer-dist laravel/laravel"

#----------------------------------------------------------
# GIT
#----------------------------------------------------------
alias gc="git clone"
alias gs="git status"
alias gcom="git commit -m"
alias gpo="git push origin"
alias gch='git checkout'
alias ga="git add ."
alias gc-ntr="git clone git@github.com:skapator/entre.git"
alias gc-eb="git clone git@github.com:skapator/element-bits.git"

#----------------------------------------------------------
# DOCKER
#----------------------------------------------------------
alias dkrc="docker-compose"
alias dkrcn="docker container"
alias dkr="docker"
alias dkrv="docker volume"

# PHP SERVER
alias phps="php -S localhost:5995"

#-------------------------------------------
#  dwm
#----------------------------------------------------------
alias dwmc="make clean && rm -f config.h && git reset --hard origin/master"
