filetype on
syntax on

set encoding=utf-8
set noerrorbells
set autoindent
set tabstop=4 
set softtabstop=2
set shiftwidth=2
set expandtab
set smartindent
set nu
set hidden
set mouse=a
set nowrap
set noswapfile
set nobackup
set incsearch
set laststatus=2
set undodir=~/.vim/undodir
set undofile
set clipboard+=unnamedplus
set splitright
set splitbelow
" set colorcolumn=80
set wildignore+=*.a,*o,*.bmp,*.jpg,*.gif,*.tiff,*.ico,*.png,*.jpeg,*.svg
set wildignore+=.idea,.DS_Store,.git,node_modules,.hg,.svn
set wildignore+=*.deb,*.AppImage


call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'sheerun/vim-polyglot'
Plug 'jwalton512/vim-blade'
Plug 'chrisbra/csv.vim'
Plug 'ekalinin/Dockerfile.vim'
Plug 'fsharp/vim-fsharp', {
      \ 'for': 'fsharp',
      \ 'do':  'make fsautocomplete',
      \}
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
Plug 'elzr/vim-json'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'chr4/nginx.vim'
Plug 'StanAngeloff/php.vim'
Plug 'digitaltoad/vim-pug'
Plug 'vim-python/python-syntax'
Plug 'cakebaker/scss-syntax.vim'
Plug 'arzg/vim-sh'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'posva/vim-vue'
Plug 'stephpy/vim-yaml'
Plug 'amadeus/vim-xml'
Plug 'phpactor/phpactor', {'for': 'php', 'branch': 'master', 'do': 'composer install --no-dev -o'}
call plug#end() 

colorscheme gruvbox
set background=dark
highlight Normal ctermbg=NONE
highlight ColorColumn ctermbg=black guibg=lightgray
highlight LineNr ctermfg=Brown

"Close vim if the only tab is NERDTree
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeShowHidden=1
let NERDTreeWinSize=24
"let NERDTreeCustomOpenArgs={'file':{'where': 't'}}
"let NERDTreeMapOpenInTab='<C-Enter>'

nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>
nmap <C-f> :NERDTreeToggle<CR>
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
imap jj <Esc>

nnoremap tn :tabnew<Space>
nnoremap th :tabprev<Enter>
nnoremap tl :tabnext<Enter>
nnoremap <Tab> :bnext<CR>

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_prompt_mappings = {
  \ 'AcceptSelection("e")': [],
  \ 'AcceptSelection("t")': ['<cr>', '<c-m>'],
  \ }

let g:javascript_plugin_jsdoc = 1

let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ }
