#!/bin/bash

#--------------------------------
# DATE
#--------------------------------
datex() {
  date "+%a, %b %d"
}

timex() {
  date "+%I:%M%p"
}

timexx() {
  date "+%H:%M"
}

#--------------------------------
# NETWORK SPEED
#--------------------------------
netspeed() {
  R1=$(cat /sys/class/net/enp*/statistics/rx_bytes)
  T1=$(cat /sys/class/net/enp*/statistics/tx_bytes)
  sleep 1
  R2=$(cat /sys/class/net/enp*/statistics/rx_bytes)
  T2=$(cat /sys/class/net/enp*/statistics/tx_bytes)
  TBPS=`expr $T2 - $T1`
  RBPS=`expr $R2 - $R1`
  TKBPS=`expr $TBPS / 1024 \* 8`
  RKBPS=`expr $RBPS / 1024 \* 8`
  echo -e "$TKBPS kB~$RKBPS kB"
}

#--------------------------------
# NETWORK TRAFFIC
#--------------------------------
nettraf() {
  rx=$(cat /sys/class/net/enp*/statistics/rx_bytes)
  tx=$(cat /sys/class/net/enp*/statistics/tx_bytes)
  echo -e "$(numfmt --to=iec $rx)~$(numfmt --to=iec $tx)"
}

#---------------------------------
# MEMORY
#--------------------------------
mem() {
  mem="$(free -h | awk '/Mem:/ {printf $3 "~" $2}')"
  echo -e "$mem"
}

#---------------------------------
# CPU
#--------------------------------

cpu() {
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
  echo -e "$cpu%"
}

#---------------------------------
# TEMPERATURE
#--------------------------------
temp() {
  tmp="$(grep temp_C ~/.config/weather.txt | awk '{print $2}' | sed 's/"//g' | sed 's/,/°C/g')"
  echo "$tmp"
}

#---------------------------------
# LOOP
#--------------------------------
SLEEP_SEC=3
while true; do     
  echo "+@fg=1;/ +@fg=6;$(netspeed) +@fg=1;/ +@fg=0;$(nettraf) +@fg=1;/ +@fg=3;$(cpu) +@fg=1;/ +@fg=2;$(mem) +@fg=1;/ +@fg=4;$(temp) +@fg=1;/ +@fg=7;$(datex) +@fg=0;$(timexx) +@fg=1;/"
  sleep $SLEEP_SEC
done
